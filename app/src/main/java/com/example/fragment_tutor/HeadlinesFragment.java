package com.example.fragment_tutor;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import androidx.annotation.NonNull;
import androidx.fragment.app.ListFragment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HeadlinesFragment extends ListFragment {

    OnHeadlineSelectedListener mCallback;
    public interface OnHeadlineSelectedListener {
        public void onArticleSelected(int position);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        int layout;

        if(Ipsum.headlines.size() == 0) {
            Ipsum.headlines.add(getResources().getString(R.string.head));
            Ipsum.article.add(getResources().getString(R.string.artc));
        }


        List<Map<String, String>> listArray = new ArrayList<>();
        for(int i = 0; i < Ipsum.headlines.size(); i++) {
            Map<String, String> listItem = new HashMap<>();
            listItem.put("titleKey", Ipsum.headlines.get(i).substring(0, 10)+"...");
            listItem.put("detailKey", Ipsum.article.get(i).substring(0, 30)+"...");
            listArray.add(listItem);
        }

        SimpleAdapter adapter = new SimpleAdapter(getActivity(), listArray,
                android.R.layout.simple_list_item_2,
                new String[] {"titleKey", "detailKey" },
                new int[] {android.R.id.text1, android.R.id.text2});
        setListAdapter(adapter);

    }

    @Override
    public void onListItemClick(@NonNull ListView l, @NonNull View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        mCallback.onArticleSelected(position);
        getListView().setItemChecked(position, true);
    }

    @Override
    public void onStart() {
        super.onStart();
        if(getFragmentManager().findFragmentById(R.id.article_fragment) != null)
            getListView().setChoiceMode(ListView.CHOICE_MODE_SINGLE);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        try {
            mCallback = (OnHeadlineSelectedListener) context;
        } catch (ClassCastException ex) {
            throw new ClassCastException(context.toString() + "must implement OnHeadlineSelectedListener");
        }
    }
}
