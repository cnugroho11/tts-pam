package com.example.fragment_tutor;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class InputNewActivity extends AppCompatActivity {

    private EditText txtHeadline;
    private EditText txtArticle;
    private Button btnSave;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input_new);

        txtHeadline = findViewById(R.id.txtHeadline);
        txtArticle = findViewById(R.id.txtArticle);
        btnSave = findViewById(R.id.btnSave);

    }

    public void save (View v) {
        String headline = txtHeadline.getText().toString();
        String article = txtArticle.getText().toString();
        if(headline.length() < 10 || article.length() < 30) {
            Context context = getApplicationContext();
            int duration = Toast.LENGTH_SHORT;

            Toast toast = Toast.makeText(context, getResources().getString(R.string.message_t), duration);
            toast.show();
        }
        else {
            Ipsum.headlines.add(headline);
            Ipsum.article.add(article);
            Intent intent = new Intent(this, MainActivity.class);
            finish();
            startActivity(intent);
        }

    }
}